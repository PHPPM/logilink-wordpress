<?php
   /*
   *
   Template Name: HomePage Tempalate
   *
   */
   get_header();
   ?>
<div class=container>
   <div data-elementor-type=wp-post data-elementor-id=5557 class="elementor elementor-5557" data-elementor-settings=[]>
      <div class=elementor-inner>
         <div class=elementor-section-wrap>
            <section class="elementor-element elementor-element-350ad2d elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=350ad2d data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div class="elementor-container elementor-column-gap-narrow">
                  <div class=elementor-row>
                     <div class="elementor-element elementor-element-cd66a25 elementor-column elementor-col-50 elementor-top-column" data-id=cd66a25 data-element_type=column>
                        <div class="elementor-column-wrap  elementor-element-populated">
                           <div class=elementor-widget-wrap>
                              <div class="elementor-element elementor-element-33ccc5b elementor-widget elementor-widget-spacer" data-id=33ccc5b data-element_type=widget data-widget_type=spacer.default>
                                 <div class=elementor-widget-container>
                                    <div class=elementor-spacer>
                                       <div class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-e2b45cc elementor-widget elementor-widget-text-editor" data-id=e2b45cc data-element_type=widget data-widget_type=text-editor.default>
                                 <div class=elementor-widget-container>
                                    <div class="elementor-text-editor elementor-clearfix">
                                       <h1 id="banner-heding">
                                          <span style="color: #ffffff;">LogiLink – the Digital Accelerator for the logistics industry.</span>
                                       </h1>
                                       <p id="banner-para">
                                          <span style="color: #ffffff;">Logilink.io is a powerful, hyper-scalable and secure integration platform (iPaaS) for SaaS companies and agencies. </span>
                                          <span style="color: #ffffff;">Create integrations with the Blend Editor, embed them in the UI of your platform, enable self-service activation and centrally manage your customers.</span>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-f82b4da elementor-widget elementor-widget-text-editor" data-id=f82b4da data-element_type=widget data-widget_type=text-editor.default>
                                 <div class=elementor-widget-container>
                                    <div class="elementor-text-editor elementor-clearfix">
                                       <p>
                                          <script src=https://fast.wistia.com/embed/medias/9awhfwy8k8.jsonp async></script>
                                          <script src=https://fast.wistia.com/assets/external/E-v1.js async></script>
                                          <span class="wistia_embed wistia_async_9awhfwy8k8 popover=true popoverContent=link" style="display: inline; position: relative;">
                                          <a style="color: #ffffff;" href=#> Watch a 2-minute video 
                                          <img class="alignnone  wp-image-5737" src=/wp-content/uploads/2019/07/play-button.png alt width=34 height=34 srcset="/wp-content/uploads/2019/07/play-button.png 512w, client/q_glossy_ret_img_w_150/wp-content/uploads/2019/07/play-button-150x150.png 150w, client/q_glossy_ret_img_w_300/wp-content/uploads/2019/07/play-button-300x300.png 300w" sizes="(max-width: 34px) 100vw, 34px">
                                          </a>
                                          </span>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-f5e3a3d elementor-widget elementor-widget-html" data-id=f5e3a3d data-element_type=widget data-widget_type=html.default>
                                 <div
                                    class=elementor-widget-container> </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-08e205d elementor-widget elementor-widget-spacer" data-id=08e205d data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-a3cf059 elementor-widget elementor-widget-spacer" data-id=a3cf059 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-c288a3e elementor-column elementor-col-50 elementor-top-column" data-id=c288a3e data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-31846a9 elementor-widget elementor-widget-spacer" data-id=31846a9 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-c8bc0f7 elementor-widget elementor-widget-image" data-id=c8bc0f7 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div class=elementor-image>
                                       <img src=wp-content/uploads/2019/07/blendr.io-ipaas-for-saas-companies.png title="logilink.io iPaaS for SaaS companies" alt="logilink.io iPaaS for SaaS companies">
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-949e77d elementor-widget elementor-widget-spacer" data-id=949e77d data-element_type=widget data-widget_type=spacer.default>
                                 <div class=elementor-widget-container>
                                    <div class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-7c79da7 elementor-element-350ad2d elementor-section-stretched elementor-hidden-desktop elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=7c79da7 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-narrow">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-056ecf8 elementor-column elementor-col-50 elementor-top-column" data-id=056ecf8 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-c11a557 elementor-widget elementor-widget-spacer" data-id=c11a557 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-efcbae5 elementor-widget elementor-widget-text-editor" data-id=efcbae5 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h1><span
                                          style="color: #ffffff;">LogiLink – the Digital Accelerator for the logistics industry.</span></h1>
                                       <p><span
                                          style="color: #ffffff;">logilink.io is a powerful, hyper-scalable and secure integration platform (iPaaS) for SaaS companies and agencies. </span><span
                                          style="color: #ffffff;">Create integrations with the Blend Editor, embed them in the UI of your platform, enable self-service activation and centrally manage your customers.</span></p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-2cd72e0 elementor-widget elementor-widget-text-editor" data-id=2cd72e0 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p>
                                          <script src=https://fast.wistia.com/embed/medias/9awhfwy8k8.jsonp async></script><script src=https://fast.wistia.com/assets/external/E-v1.js async></script><span
                                             class="wistia_embed wistia_async_9awhfwy8k8 popover=true popoverContent=link" style="display: inline; position: relative;"><a
                                             style="color: #ffffff;" href=#> Watch a 2-minute video <img
                                             class="alignnone  wp-image-5737" src=/wp-content/uploads/2019/07/play-button.png alt width=34 height=34 srcset="/wp-content/uploads/2019/07/play-button.png 512w, client/q_glossy_ret_img_w_150/wp-content/uploads/2019/07/play-button-150x150.png 150w, client/q_glossy_ret_img_w_300/wp-content/uploads/2019/07/play-button-300x300.png 300w" sizes="(max-width: 34px) 100vw, 34px"></a></span>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-6b167b3 elementor-widget elementor-widget-html" data-id=6b167b3 data-element_type=widget data-widget_type=html.default>
                                 <div
                                    class=elementor-widget-container>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-aa15b5c elementor-widget elementor-widget-spacer" data-id=aa15b5c data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-7d45c36 elementor-column elementor-col-50 elementor-top-column" data-id=7d45c36 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-980de03 elementor-widget elementor-widget-image" data-id=980de03 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src="wp-content/uploads/2019/07/blendr.io-ipaas-for-saas-companies.png" title="logilink.io iPaaS for SaaS companies" alt="logilink.io iPaaS for SaaS companies">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-dbb1777 elementor-widget elementor-widget-spacer" data-id=dbb1777 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-3f5ed7b elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=3f5ed7b data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-c7c530b elementor-column elementor-col-100 elementor-top-column" data-id=c7c530b data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-c8fa766 elementor-widget elementor-widget-spacer" data-id=c8fa766 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-0e159c7 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=0e159c7 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-340087a elementor-column elementor-col-100 elementor-top-column" data-id=340087a data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-457edef elementor-widget elementor-widget-text-editor" data-id=457edef data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2 style="text-align: center;">Rapidly growing SaaS companies and established businesses trust logilink.io</h2>
                                       <p class="text-new-psd-wala">Logilink's iPAAs (integrated platform as a service) specializes in logistics and E-commerce connectivity</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-c8e5bad elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=c8e5bad data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-18e4c85 elementor-column elementor-col-16 elementor-top-column" data-id=18e4c85 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-96496de elementor-widget elementor-widget-spacer" data-id=96496de data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-6c573f7 elementor-widget elementor-widget-image" data-id=6c573f7 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <!-- <img
                                          width=400 height=135 src=client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/qualifio.png class="attachment-full size-full" alt srcset="client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/qualifio.png 400w, client/q_glossy_ret_img_w_300/wp-content/uploads/2018/11/qualifio-300x101.png 300w" sizes="(max-width: 400px) 100vw, 400px"> -->
                                       <img width=200 height=113 src=wp-content/uploads/2020/05/Qualifio.png class="attachment-full size-full" alt>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-6660182 elementor-column elementor-col-16 elementor-top-column" data-id=6660182 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-cf7cac0 elementor-widget elementor-widget-spacer" data-id=cf7cac0 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-69d62a2 elementor-widget elementor-widget-image" data-id=69d62a2 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=https://www.sortlist.be/en/ target=_blank>
                                          <!-- <img
                                             width=400 height=135 src=client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/sortlist.png class="attachment-full size-full" alt srcset="client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/sortlist.png 400w, client/q_glossy_ret_img_w_300/wp-content/uploads/2018/11/sortlist-300x101.png 300w" sizes="(max-width: 400px) 100vw, 400px">	 -->
                                          <img width=200 height=113 src=wp-content/uploads/2020/05/sortlist.png class="attachment-full size-full" alt>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-7c5b1c7 elementor-column elementor-col-16 elementor-top-column" data-id=7c5b1c7 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-f0b7607 elementor-widget elementor-widget-spacer" data-id=f0b7607 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-a7d3f3e elementor-widget elementor-widget-image" data-id=a7d3f3e data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <!-- <img
                                          width=400 height=135 src=client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/accenture.png class="attachment-full size-full" alt srcset="client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/accenture.png 400w, client/q_glossy_ret_img_w_300/wp-content/uploads/2018/11/accenture-300x101.png 300w" sizes="(max-width: 400px) 100vw, 400px"> -->
                                       <img width=200 height=113 src=wp-content/uploads/2020/05/accenture.png class="attachment-full size-full" alt>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-a7aa56c elementor-column elementor-col-16 elementor-top-column" data-id=a7aa56c data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-5c4a630 elementor-widget elementor-widget-spacer" data-id=5c4a630 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-0a2d7f9 elementor-widget elementor-widget-image" data-id=0a2d7f9 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <!-- <img
                                          width=400 height=135 src=client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/wheels-co.png class="attachment-full size-full" alt srcset="client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/wheels-co.png 400w, client/q_glossy_ret_img_w_300/wp-content/uploads/2018/11/wheels-co-300x101.png 300w" sizes="(max-width: 400px) 100vw, 400px"> -->
                                       <img width=200 height=113 src=wp-content/uploads/2020/05/wheels-co.png class="attachment-full size-full" alt>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-c1e6146 elementor-column elementor-col-16 elementor-top-column" data-id=c1e6146 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-c85969a elementor-widget elementor-widget-spacer" data-id=c85969a data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-df2ff3c elementor-widget elementor-widget-image" data-id=df2ff3c data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <!-- <img
                                          width=400 height=135 src=client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/vlerick.png class="attachment-full size-full" alt srcset="client/q_glossy_ret_img_w_400/wp-content/uploads/2018/11/vlerick.png 400w, client/q_glossy_ret_img_w_300/wp-content/uploads/2018/11/vlerick-300x101.png 300w" sizes="(max-width: 400px) 100vw, 400px"> -->
                                       <img width=200 height=113 src=wp-content/uploads/2020/05/Vlerick.png class="attachment-full size-full" alt>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-a6df4f1 elementor-column elementor-col-16 elementor-top-column" data-id=a6df4f1 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-375ea35 elementor-widget elementor-widget-image" data-id=375ea35 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img width=200 height=113 src=wp-content/uploads/2020/04/leadstreet-200.png class="attachment-full size-full" alt>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-b85ec6b elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=b85ec6b data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-409818d elementor-column elementor-col-100 elementor-top-column" data-id=409818d data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-a612c6b elementor-widget elementor-widget-spacer" data-id=a612c6b data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="custom-green-background elementor-element elementor-element-59ad755 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=59ad755 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div class=elementor-row>
                     <div
                        class="elementor-element elementor-element-873fac6 elementor-column elementor-col-100 elementor-top-column" data-id=873fac6 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-5a00ff0 elementor-widget elementor-widget-spacer" data-id=5a00ff0 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-3ffdc5c elementor-widget elementor-widget-text-editor" data-id=3ffdc5c data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2 style="text-align: center;"><span class="bluish">Accelerate your digital transformation with the LogiLink iPaaS</span></h2>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-236f0e8 elementor-widget elementor-widget-spacer" data-id=236f0e8 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="custom-green-background elementor-element elementor-element-467ec6f elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=467ec6f data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div class="elementor-container elementor-column-gap-wide">
                  <div class=elementor-row>
                     <div
                        class="elementor-element elementor-element-30f23ed elementor-column elementor-col-25 elementor-top-column" data-id=30f23ed data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e1259d2 elementor-widget elementor-widget-image" data-id=e1259d2 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Cloud-Systems.png title=clock alt=clock>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-0e4de1b elementor-column elementor-col-25 elementor-top-column" data-id=0e4de1b data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-f0934a3 elementor-widget elementor-widget-text-editor" data-id=f0934a3 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">Cloud Systems – Your systems transferred to the Cloud – access anywhere, anytime and any how.</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-8183e2f elementor-column elementor-col-25 elementor-top-column" data-id=8183e2f data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-dc55015 elementor-widget elementor-widget-image" data-id=dc55015 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Eco-Systems.png title="use cases" alt="use cases">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-7365d45 elementor-column elementor-col-25 elementor-top-column" data-id=7365d45 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-ce9d10c elementor-widget elementor-widget-text-editor" data-id=ce9d10c data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">Eco-Systems –  your corporate eco-system in one framework</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="custom-green-background elementor-element elementor-element-e4c07e7 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=e4c07e7 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wide">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-7babc31 elementor-column elementor-col-25 elementor-top-column" data-id=7babc31 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-87b63c3 elementor-widget elementor-widget-image" data-id=87b63c3 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Connectivity.png title=connectors alt=connectors>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-fb30a80 elementor-column elementor-col-25 elementor-top-column" data-id=fb30a80 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-6c64082 elementor-widget elementor-widget-text-editor" data-id=6c64082 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">Connectivity – integrate all your legacy systems with new tools and functionality</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-b3d5c64 elementor-column elementor-col-25 elementor-top-column" data-id=b3d5c64 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-3322f1d elementor-widget elementor-widget-image" data-id=3322f1d data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/App-store.png title=develop alt=develop>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-4d22ad9 elementor-column elementor-col-25 elementor-top-column" data-id=4d22ad9 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-2d5e28a elementor-widget elementor-widget-text-editor" data-id=2d5e28a data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">App store – extensive range of software options</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="custom-green-background elementor-element elementor-element-301f115 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=301f115 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wide">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-6bb3e1d elementor-column elementor-col-25 elementor-top-column" data-id=6bb3e1d data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-2e5816e elementor-widget elementor-widget-image" data-id=2e5816e data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Networking.png title=money alt=money>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-3752f8e elementor-column elementor-col-25 elementor-top-column" data-id=3752f8e data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-50707a9 elementor-widget elementor-widget-text-editor" data-id=50707a9 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">Networking – network your clients and suppliers</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-f5d7772 elementor-column elementor-col-25 elementor-top-column" data-id=f5d7772 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-2273b90 elementor-widget elementor-widget-image" data-id=2273b90 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/configuration.png title=software alt=software>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-ee304e7 elementor-column elementor-col-25 elementor-top-column" data-id=ee304e7 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-aa604cc elementor-widget elementor-widget-text-editor" data-id=aa604cc data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">Configuration – easy and rapid configuration – low code IT environment</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>

            <section class="custom-green-background elementor-element elementor-element-467ec6f elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=467ec6f data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div class="elementor-container elementor-column-gap-wide">
                  <div class=elementor-row>
                     <div
                        class="elementor-element elementor-element-30f23ed elementor-column elementor-col-25 elementor-top-column" data-id=30f23ed data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e1259d2 elementor-widget elementor-widget-image" data-id=e1259d2 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Plug-n-play.png title=clock alt=clock>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-0e4de1b elementor-column elementor-col-25 elementor-top-column" data-id=0e4de1b data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-f0934a3 elementor-widget elementor-widget-text-editor" data-id=f0934a3 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">Plug-n-play – switch in and out and on and off apps as required</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-8183e2f elementor-column elementor-col-25 elementor-top-column" data-id=8183e2f data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-dc55015 elementor-widget elementor-widget-image" data-id=dc55015 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Flexible.png title="use cases" alt="use cases">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-7365d45 elementor-column elementor-col-25 elementor-top-column" data-id=7365d45 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-ce9d10c elementor-widget elementor-widget-text-editor" data-id=ce9d10c data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">Flexible  – enterprise or transactional model</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>




            <section class="custom-green-background elementor-element elementor-element-467ec6f elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=467ec6f data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div class="elementor-container elementor-column-gap-wide">
                  <div class=elementor-row>
                     <div
                        class="elementor-element elementor-element-30f23ed elementor-column elementor-col-25 elementor-top-column" data-id=30f23ed data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e1259d2 elementor-widget elementor-widget-image" data-id=e1259d2 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Cost-effective.png title=clock alt=clock>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-0e4de1b elementor-column elementor-col-25 elementor-top-column" data-id=0e4de1b data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-f0934a3 elementor-widget elementor-widget-text-editor" data-id=f0934a3 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3><span
                                          style="color: #333;">Cost effective – managed, supported, transactional or selective services</span></h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>




            <section class="custom-green-background elementor-element elementor-element-c554fe2 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=c554fe2 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-3b1d810 elementor-column elementor-col-100 elementor-top-column" data-id=3b1d810 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-c19d487 elementor-widget elementor-widget-spacer" data-id=c19d487 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-41f5a48 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=41f5a48 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-ee892ed elementor-column elementor-col-100 elementor-top-column" data-id=ee892ed data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-8e6d284 elementor-widget elementor-widget-spacer" data-id=8e6d284 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-26c1885 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=26c1885 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div
                     class=elementor-row>
                     <div
                        class="elementor-element elementor-element-944a7c6 elementor-column elementor-col-100 elementor-top-column" data-id=944a7c6 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-66b737a elementor-widget elementor-widget-text-editor" data-id=66b737a data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2 style="text-align: center;">Boost your integration capabilities with iPaaS</h2>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-a61f58a elementor-widget elementor-widget-text-editor" data-id=a61f58a data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p
                                          style="text-align: center;">Discover integration solutions for Event Tech, Martech, Cloud Communications, other SaaS providers and agencies.</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-29b65d1 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=29b65d1 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider ipass-sec">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-d2e4de4 elementor-column elementor-col-33 elementor-top-column" data-id=d2e4de4 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-6234953 elementor-widget elementor-widget-image" data-id=6234953 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=/integrate-voip/ >
                                       <img src=wp-content/uploads/2020/04/img-cloud-communications-3.png title=cloud-communications-integrations alt=cloud-communications-integrations>		</a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-bfe5181 elementor-widget elementor-widget-text-editor" data-id=bfe5181 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;">Cloud Communications</h3>
                                       <p
                                          style="text-align: center;">Integrate your CloudCom platform with with CRM’s, helpdesks, and other cloud apps.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-adaa9ae elementor-align-center elementor-widget elementor-widget-button" data-id=adaa9ae data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/integrate-voip/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-0885d63 elementor-column elementor-col-33 elementor-top-column" data-id=0885d63 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-99ec107 elementor-widget elementor-widget-image" data-id=99ec107 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=/event-technology-integration/ >
                                          <!-- <img
                                             width=500 height=378 src="<?php echo site_url(); ?>/wp-content/uploads/2020/04/img-event-tech-saas-v3.png" class="attachment-large size-large" alt srcset="client/q_glossy_ret_img_w_500/wp-content/uploads/2020/04/img-event-tech-saas-v3.png 500w, client/q_glossy_ret_img_w_300/wp-content/uploads/2020/04/img-event-tech-saas-v3-300x227.png 300w" sizes="(max-width: 500px) 100vw, 500px">	 -->	
                                          <img src=wp-content/uploads/2020/04/img-event-tech-saas-v3.png title=cloud-communications-integrations alt=cloud-communications-integrations>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-d309a88 elementor-widget elementor-widget-text-editor" data-id=d309a88 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;">Event Tech SaaS</h3>
                                       <p
                                          style="text-align: center;">Integrate your Event Tech platform with mobile apps, EMS, marketing tools &amp; other tools.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-47c26e1 elementor-align-center elementor-widget elementor-widget-button" data-id=47c26e1 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/event-technology-integration/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-aaaeae7 elementor-column elementor-col-33 elementor-top-column" data-id=aaaeae7 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-a28f0c9 elementor-widget elementor-widget-image" data-id=a28f0c9 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=/integrate-saas-platform/ >
                                          <!-- <img
                                             width=500 height=378 src=wp-content/uploads/2020/04/img-marketing-technology-v3.png class="attachment-large size-large" alt srcset="client/q_glossy_ret_img_w_500/wp-content/uploads/2020/04/img-marketing-technology-v3.png 500w, client/q_glossy_ret_img_w_300/wp-content/uploads/2020/04/img-marketing-technology-v3-300x227.png 300w" sizes="(max-width: 500px) 100vw, 500px"> -->	
                                          <img src=wp-content/uploads/2020/04/img-marketing-technology-v3.png title=cloud-communications-integrations alt=cloud-communications-integrations>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-ba30696 elementor-widget elementor-widget-text-editor" data-id=ba30696 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;">Martech SaaS</h3>
                                       <p
                                          style="text-align: center;">Integrate your platform with CRM, leadgen forms, webshops and other cloud apps.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-fd63f3c elementor-align-center elementor-widget elementor-widget-button" data-id=fd63f3c data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/integrate-saas-platform/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-713ed76 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=713ed76 data-element_type=section>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-bb6f3be elementor-column elementor-col-100 elementor-top-column" data-id=bb6f3be data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-fb8bc26 elementor-widget elementor-widget-spacer" data-id=fb8bc26 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-5c2cafb elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=5c2cafb data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider ipass-sec">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-e366e78 elementor-column elementor-col-33 elementor-top-column" data-id=e366e78 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-8f41166 elementor-widget elementor-widget-image" data-id=8f41166 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=/build-integrations/ >
                                       <img
                                          src=wp-content/uploads/2020/04/img-agencies-v3.png title=integrations-agencies alt=integrations-agencies>		</a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-f5f1053 elementor-widget elementor-widget-text-editor" data-id=f5f1053 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;">Integrators &amp; Agencies</h3>
                                       <p
                                          style="text-align: center;">Build tailored integrations for your customers. Become a certified logilink.io partner.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7c54400 elementor-align-center elementor-widget elementor-widget-button" data-id=7c54400 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/build-integrations/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-61b9d9d elementor-column elementor-col-33 elementor-top-column" data-id=61b9d9d data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-7fbc96c elementor-widget elementor-widget-image" data-id=7fbc96c data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=/accounting-software-integration/ >
                                          <!-- <img
                                             width=500 height=378 src=wp-content/uploads/2020/04/img-accounting-v3.png class="attachment-large size-large" alt srcset="client/q_glossy_ret_img_w_500/wp-content/uploads/2020/04/img-accounting-v3.png 500w, client/q_glossy_ret_img_w_300/wp-content/uploads/2020/04/img-accounting-v3-300x227.png 300w" sizes="(max-width: 500px) 100vw, 500px"> -->	
                                          <img src=wp-content/uploads/2020/04/img-accounting-v3.png title=cloud-communications-integrations alt=cloud-communications-integrations>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-a628897 elementor-widget elementor-widget-text-editor" data-id=a628897 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;">Accounting Software</h3>
                                       <p
                                          style="text-align: center;">Integrate accounting software with e-commerce platforms, ERPs, and other cloud solutions.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-4b4b3f2 elementor-align-center elementor-widget elementor-widget-button" data-id=4b4b3f2 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/accounting-software-integration/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-3b19dfa elementor-column elementor-col-33 elementor-top-column" data-id=3b19dfa data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-95969bc elementor-widget elementor-widget-image" data-id=95969bc data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=/ai-saas-integrations/ >
                                          <!-- <img
                                             width=500 height=378 src=wp-content/uploads/2020/04/img-b2b-saas-v3.png class="attachment-large size-large" alt srcset="client/q_glossy_ret_img_w_500/wp-content/uploads/2020/04/img-b2b-saas-v3.png 500w, client/q_glossy_ret_img_w_300/wp-content/uploads/2020/04/img-b2b-saas-v3-300x227.png 300w" sizes="(max-width: 500px) 100vw, 500px">	
                                              -->
                                          <img src=wp-content/uploads/2020/04/img-b2b-saas-v3.png title=cloud-communications-integrations alt=cloud-communications-integrations>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-3d499d2 elementor-widget elementor-widget-text-editor" data-id=3d499d2 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;">AI-driven SaaS</h3>
                                       <p
                                          style="text-align: center;">Aconnect your AI-driven platform to 500+ marketing, sales, HR and other cloud platforms.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-00b54f5 elementor-align-center elementor-widget elementor-widget-button" data-id=00b54f5 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/ai-saas-integrations/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-7713ba4 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=7713ba4 data-element_type=section>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-0883420 elementor-column elementor-col-100 elementor-top-column" data-id=0883420 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-932c5bd elementor-widget elementor-widget-spacer" data-id=932c5bd data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-dec9894 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=dec9894 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider ipass-sec">
                  <div class="elementor-row custom-section-class">
                 <!--     <div
                        class="elementor-element elementor-element-1a4984b elementor-column elementor-col-33 elementor-top-column" data-id=1a4984b data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class=elementor-column-wrap>
                           <div
                              class=elementor-widget-wrap></div>
                        </div>
                     </div> -->
                     <div
                        class="elementor-element elementor-element-3381827 elementor-column elementor-col-33 elementor-top-column" data-id=3381827 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-d8dc453 elementor-widget elementor-widget-image" data-id=d8dc453 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=/accounting-software-integration/ >
                                          <!-- <img
                                             width=500 height=378 src=wp-content/uploads/2020/05/img-cv-profiles-video-v2.png class="attachment-large size-large" alt srcset="client/q_glossy_ret_img_w_500/wp-content/uploads/2020/05/img-cv-profiles-video-v2.png 500w, client/q_glossy_ret_img_w_300/wp-content/uploads/2020/05/img-cv-profiles-video-v2-300x227.png 300w" sizes="(max-width: 500px) 100vw, 500px">
                                              -->
                                          <img src=wp-content/uploads/2020/04/img-cv-profiles-video-v2.png title=cloud-communications-integrations alt=cloud-communications-integrations>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-0d8ae1c elementor-widget elementor-widget-text-editor" data-id=0d8ae1c data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;">HRM Platforms</h3>
                                       <p
                                          style="text-align: center;">Integrate ATS and HRMS platforms, assessment tools, and other cloud platforms.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-a3572ce elementor-align-center elementor-widget elementor-widget-button" data-id=a3572ce data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/accounting-software-integration/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-77b857b elementor-column elementor-col-33 elementor-top-column" data-id=77b857b data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class=elementor-column-wrap>
                           <div
                              class=elementor-widget-wrap></div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-a4a01c6 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=a4a01c6 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-d82ee64 elementor-column elementor-col-100 elementor-top-column" data-id=d82ee64 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-b239339 elementor-widget elementor-widget-spacer" data-id=b239339 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="custom-green-background elementor-element elementor-element-91bf0f9 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=91bf0f9 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-441cf94 elementor-column elementor-col-50 elementor-top-column" data-id=441cf94 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-08b9948 elementor-widget elementor-widget-spacer" data-id=08b9948 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-03c978f elementor-widget elementor-widget-text-editor" data-id=03c978f data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2><span class="bluish">LogiLink Introduction and Starter Options</span></h2>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-993b094 elementor-widget elementor-widget-text-editor" data-id=993b094 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p
                                          style="text-align: left;"><span
                                          style="color: #333333;">Logilink provides enterprise, managed, supported and transactional packages. We can show you the options as well as demonstrate how you can empower your own technical teams to build, test and launch integrations with the LogiLink blend editor.</span></p>
                                     <!--   <p><span
                                          style="color: #333333;">In this webinar we will cover how you can build comprehensive integrations with the Blend Editor.</span></p> -->
                                    </div>
                                 </div>
                              </div>
                       <!--        <div
                                 class="elementor-element elementor-element-9d39926 elementor-align-left elementor-widget elementor-widget-button" data-id=9d39926 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/webinar/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Select time</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div> -->
                              <div
                                 class="elementor-element elementor-element-2927b76 elementor-widget elementor-widget-text-editor" data-id=2927b76 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-1804795 elementor-column elementor-col-50 elementor-top-column" data-id=1804795 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-84d4a56 elementor-widget elementor-widget-spacer" data-id=84d4a56 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-fd1376c elementor-widget elementor-widget-image" data-id=fd1376c data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=/webinar/ >
                                          <!-- <img
                                             width=877 height=634 src=wp-content/uploads/2020/04/logilink.io-webinar.png class="attachment-large size-large" alt srcset="client/q_glossy_ret_img_w_877/wp-content/uploads/2020/04/logilink.io-webinar.png 877w, client/q_glossy_ret_img_w_300/wp-content/uploads/2020/04/logilink.io-webinar-300x217.png 300w, client/q_glossy_ret_img_w_768/wp-content/uploads/2020/04/logilink.io-webinar-768x555.png 768w" sizes="(max-width: 877px) 100vw, 877px">	 -->
                                          <img src=wp-content/uploads/2020/05/Blendr.io-webinar.png title=cloud-communications-integrations alt=cloud-communications-integrations>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-4dddd6e elementor-widget elementor-widget-spacer" data-id=4dddd6e data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-cdf91fb elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=cdf91fb data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-e16aa72 elementor-column elementor-col-100 elementor-top-column" data-id=e16aa72 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-f6d809b elementor-widget elementor-widget-spacer" data-id=f6d809b data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-04eb599 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=04eb599 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-c3e9a1c elementor-column elementor-col-50 elementor-top-column" data-id=c3e9a1c data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-7b62a57 elementor-widget elementor-widget-image" data-id=7b62a57 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <!-- <img
                                          width=500 height=378 src=client/q_glossy_ret_img_w_500/wp-content/uploads/2020/05/img-security.png class="attachment-large size-large" alt srcset="client/q_glossy_ret_img_w_500/wp-content/uploads/2020/05/img-security.png 500w, client/q_glossy_ret_img_w_300/wp-content/uploads/2020/05/img-security-300x227.png 300w" sizes="(max-width: 500px) 100vw, 500px">
                                           -->
                                       <img src=wp-content/uploads/2020/05/img-security.png title=cloud-communications-integrations alt=cloud-communications-integrations>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-273b83e elementor-column elementor-col-50 elementor-top-column" data-id=273b83e data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-036d144 elementor-widget elementor-widget-text-editor" data-id=036d144 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>Data Lake</h2>
                                       <p> Secure and private to latest industry standards. The LogiLink data lake segments and secures all data. Systems are robust, fast and with 99% uptime. Back-ups in real time enable swift recovery time.</p>
                                       <!-- <p>Process your data in a compliant, secure and private way with the logilink.io best-in-class procedures and practices.</p> -->
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-b3fc7c6 elementor-widget elementor-widget-button" data-id=b3fc7c6 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=/compliance-data-security-and-privacy/ class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-1a7fa29 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=1a7fa29 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-ed75961 elementor-column elementor-col-100 elementor-top-column" data-id=ed75961 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-44617d6 elementor-widget elementor-widget-spacer" data-id=44617d6 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="custom-green-background elementor-element elementor-element-cd99418 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=cd99418 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;gradient&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-bccf5c2 elementor-column elementor-col-100 elementor-top-column" data-id=bccf5c2 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-08336af elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=08336af data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <!-- <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div> -->
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-c50ea4e elementor-widget elementor-widget-text-editor" data-id=c50ea4e data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2 style="text-align: center;"><span
                                          style="color: #d81a5e;">Discover 4 main layers of the logilink.io iPaaS</span></h2>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="custom-green-background elementor-element elementor-element-e114217 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=e114217 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;gradient&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-f1d72d1 elementor-column elementor-col-25 elementor-top-column" data-id=f1d72d1 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-dd20079 elementor-widget elementor-widget-image" data-id=dd20079 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=index.html >
                                       <img
                                          src=wp-content/uploads/elementor/thumbs/white-connectivity-layer-o7v2qyj7tbl13zqlaabccei01dz8lucrr7lr719reo.png title="white connectivity layer" alt="white connectivity layer">		</a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-c6f439d elementor-widget elementor-widget-text-editor" data-id=c6f439d data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: ##d81a5d;">Connectivity layer</span></h3>
                                    </div>
                                 </div>
                              </div>
                         <!--      <div
                                 class="elementor-element elementor-element-998dd70 elementor-widget elementor-widget-spacer" data-id=998dd70 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-0afa88a elementor-column elementor-col-25 elementor-top-column" data-id=0afa88a data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-7a6aafd elementor-widget elementor-widget-image" data-id=7a6aafd data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=index.html >
                                       <img
                                          src=wp-content/uploads/elementor/thumbs/integration-layer-o7v2t1qb02fywspd36thvvivk7olpmnapjsll669kw.png title="Integration layer" alt="Integration layer">		</a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-fdb9f11 elementor-widget elementor-widget-text-editor" data-id=fdb9f11 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: ##d81a5d;">Integration layer</span></a></h3>
                                    </div>
                                 </div>
                              </div>
                         <!--      <div
                                 class="elementor-element elementor-element-3099f36 elementor-widget elementor-widget-spacer" data-id=3099f36 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-b71260f elementor-column elementor-col-25 elementor-top-column" data-id=b71260f data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-dc586ab elementor-widget elementor-widget-image" data-id=dc586ab data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=index.html >
                                       <img
                                          src=wp-content/uploads/elementor/thumbs/white-ui-embedding-layer-o7v2thpk891ue625hvq5k9hpnrhucheqfqvuqvikn4.png title="white UI &#038; embedding layer" alt="white UI & embedding layer">		</a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-b689689 elementor-widget elementor-widget-text-editor" data-id=b689689 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: #d81a5d;">UI &amp; embedding layer</span></a></h3>
                                    </div>
                                 </div>
                              </div>
                          <!--     <div
                                 class="elementor-element elementor-element-90020c2 elementor-widget elementor-widget-spacer" data-id=90020c2 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-62c5b28 elementor-column elementor-col-25 elementor-top-column" data-id=62c5b28 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-be43749 elementor-widget elementor-widget-image" data-id=be43749 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=index.html >
                                       <img
                                          src=wp-content/uploads/elementor/thumbs/management-layer-v2-o7v33szt7t63s52u9u7sf5ssayntsxcpisknb682dc.png title="Management layer v2" alt="Management layer v2">		</a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-0b392fa elementor-widget elementor-widget-text-editor" data-id=0b392fa data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color:#d81a5d;">Management layer</span></a></h3>
                                    </div>
                                 </div>
                              </div>
                            <!--   <div
                                 class="elementor-element elementor-element-f632b28 elementor-widget elementor-widget-spacer" data-id=f632b28 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-1d516d5 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=1d516d5 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-1ca0ca3 elementor-column elementor-col-50 elementor-top-column" data-id=1ca0ca3 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-9e03c53 elementor-widget elementor-widget-spacer" data-id=9e03c53 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-ce42369 elementor-widget elementor-widget-text-editor" data-id=ce42369 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>OPEN a service channel with Logicon</h2>
                                       <p>logilink.io has a rapidly growing library of connected applications. Join the logilink.io ecosystem and integrate your platform with 500+ marketing, sales, and events cloud tools.</p>
                                       <p>Did not find the connector you need? The logilink.io team will add it within a couple of days.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-6f416a7 elementor-widget elementor-widget-button" data-id=6f416a7 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html target=_blank class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-1de3377 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=1de3377 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-7b025c1 elementor-column elementor-col-50 elementor-top-column" data-id=7b025c1 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-9b7e51f elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=9b7e51f data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-caed7a5 elementor-widget elementor-widget-image" data-id=caed7a5 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=index.html >
                                       <img
                                          src=wp-content/uploads/2019/08/blendr.io-300-connectors.png title="logilink.io 300 connectors" alt="logilink.io 300 connectors">		</a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-d7b8b70 elementor-widget elementor-widget-text-editor" data-id=d7b8b70 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p><a
                                          name=blendeditor></a></p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-bf9e974 elementor-widget elementor-widget-spacer" data-id=bf9e974 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-24cbdae elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=24cbdae data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-30c869f elementor-column elementor-col-50 elementor-top-column" data-id=30c869f data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-fe6103d elementor-widget elementor-widget-spacer" data-id=fe6103d data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-f149a87 elementor-widget elementor-widget-image" data-id=f149a87 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=wp-content/uploads/2019/08/integration-layer.png title="Integration layer" alt="Integration layer">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-10bf863 elementor-widget elementor-widget-text-editor" data-id=10bf863 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p><a
                                          name=templates></a></p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-a4328e8 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=a4328e8 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-55d78fc elementor-column elementor-col-50 elementor-top-column" data-id=55d78fc data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-92f9aec elementor-widget elementor-widget-spacer" data-id=92f9aec data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-aa1d50d elementor-widget elementor-widget-text-editor" data-id=aa1d50d data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>CONNECT to LogiLink</h2>
                                       <p>The Blend Editor is a visual, low-code integration builder. Build integrations between your SaaS solution and other cloud apps by connecting data sources, applying conditions, calling webhooks, adding loops and other powerful features.</p>
                                       <p>logilink.io supports big data as well as hyper-scalable and distributed execution to manage billions of data records.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-00f51c7 elementor-widget elementor-widget-button" data-id=00f51c7 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-60a1a19 elementor-widget elementor-widget-spacer" data-id=60a1a19 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-0f89bcb elementor-section-stretched elementor-hidden-desktop elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=0f89bcb data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-4dc7174 elementor-hidden-desktop elementor-hidden-tablet elementor-column elementor-col-50 elementor-top-column" data-id=4dc7174 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-53aae90 elementor-widget elementor-widget-spacer" data-id=53aae90 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7dd9478 elementor-widget elementor-widget-text-editor" data-id=7dd9478 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>CONNECT to LogiLink</h2>
                                       <p>The Blend Editor is a visual, low-code integration builder. Build integrations between your SaaS solution and other cloud apps by connecting data sources, applying conditions, calling webhooks, adding loops and other powerful features.</p>
                                       <p>logilink.io supports big data as well as hyper-scalable and distributed execution to manage billions of data records.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-1acab66 elementor-widget elementor-widget-button" data-id=1acab66 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-1bae3cf elementor-widget elementor-widget-spacer" data-id=1bae3cf data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-8ebe463 elementor-column elementor-col-50 elementor-top-column" data-id=8ebe463 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-b9b1387 elementor-widget elementor-widget-spacer" data-id=b9b1387 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-c508a3e elementor-widget elementor-widget-image" data-id=c508a3e data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=wp-content/uploads/elementor/thumbs/integration-layer-obvjvcrck9cgss1ljya4h4abtn7gl6xin6d8mr5wms.png title="Integration layer" alt="Integration layer">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-8a630be elementor-widget elementor-widget-text-editor" data-id=8a630be data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p><a
                                          name=templates></a></p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-e8cd614 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=e8cd614 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section
               class="elementor-element elementor-element-e3790be elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=e3790be data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-c856c6b elementor-column elementor-col-50 elementor-top-column" data-id=c856c6b data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-cb071bc elementor-widget elementor-widget-spacer" data-id=cb071bc data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-addc495 elementor-widget elementor-widget-text-editor" data-id=addc495 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>APP STORE – view the menu options of apps </h2>
                                       <p>Add all the integrations you&#8217;ve built with the Blend Editor as native integrations to your SaaS solution.</p>
                                       <p>Embed integrations using iframe with SSO (single sign-on) and let your customers activate integrations with a couple of clicks straight away from your platform.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-2b5dc89 elementor-widget elementor-widget-button" data-id=2b5dc89 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-b370646 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=b370646 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-d164bf9 elementor-column elementor-col-50 elementor-top-column" data-id=d164bf9 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-773dbf4 elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=773dbf4 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7d8d09b elementor-widget elementor-widget-image" data-id=7d8d09b data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=wp-content/uploads/2020/04/embedded-integrations.png title="embedded integrations" alt="embedded integrations">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-eaa0b9c elementor-widget elementor-widget-text-editor" data-id=eaa0b9c data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p><a
                                          name=integrations></a></p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-8cb20a3 elementor-widget elementor-widget-spacer" data-id=8cb20a3 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>








































            <section
               class="elementor-element elementor-element-ff79371 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=ff79371 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-f5dc0bb elementor-column elementor-col-50 elementor-top-column" data-id=f5dc0bb data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e9d4d5c elementor-widget elementor-widget-spacer" data-id=e9d4d5c data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-e7152bf elementor-widget elementor-widget-image" data-id=e7152bf data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=wp-content/uploads/2019/08/management-layer-blendr.io_.png title="management layer logilink.io" alt="management layer logilink.io">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-e13dd1d elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=e13dd1d data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-a704d14 elementor-column elementor-col-50 elementor-top-column" data-id=a704d14 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e0a0134 elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=e0a0134 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7683026 elementor-widget elementor-widget-text-editor" data-id=7683026 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>INTEGRATE – plug-and-play to start to build your ‘eco-system’</h2>
                                       <p>Easily manage your customers, integrations and the way you embed them into your SaaS platform. logilink.io is a flexible and powerful integration platform that solves the integration challenges for all types of customers.</p>
                                       <p>Use logilink.io to embed self-service &amp; high-volume integrations to your platform or build custom integrations for your enterprise-level customers with specific needs.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7eeea1d elementor-widget elementor-widget-button" data-id=7eeea1d data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-b56bcd7 elementor-widget elementor-widget-spacer" data-id=b56bcd7 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>









                        <section
               class="elementor-element elementor-element-1d516d5 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=1d516d5 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-1ca0ca3 elementor-column elementor-col-50 elementor-top-column" data-id=1ca0ca3 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-9e03c53 elementor-widget elementor-widget-spacer" data-id=9e03c53 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-ce42369 elementor-widget elementor-widget-text-editor" data-id=ce42369 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>ECO-SYSTEM Configuration of selected apps and tools</h2>
                                       <p>logilink.io has a rapidly growing library of connected applications. Join the logilink.io ecosystem and integrate your platform with 500+ marketing, sales, and events cloud tools.</p>
                                       <p>Did not find the connector you need? The logilink.io team will add it within a couple of days.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-6f416a7 elementor-widget elementor-widget-button" data-id=6f416a7 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html target=_blank class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-1de3377 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=1de3377 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-7b025c1 elementor-column elementor-col-50 elementor-top-column" data-id=7b025c1 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-9b7e51f elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=9b7e51f data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-caed7a5 elementor-widget elementor-widget-image" data-id=caed7a5 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=index.html >
                                       <img
                                          src=wp-content/uploads/2019/08/blendr.io-300-connectors.png title="logilink.io 300 connectors" alt="logilink.io 300 connectors">    </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-d7b8b70 elementor-widget elementor-widget-text-editor" data-id=d7b8b70 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p><a
                                          name=blendeditor></a></p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-bf9e974 elementor-widget elementor-widget-spacer" data-id=bf9e974 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>




<section
               class="elementor-element elementor-element-ff79371 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=ff79371 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-f5dc0bb elementor-column elementor-col-50 elementor-top-column" data-id=f5dc0bb data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e9d4d5c elementor-widget elementor-widget-spacer" data-id=e9d4d5c data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-e7152bf elementor-widget elementor-widget-image" data-id=e7152bf data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=wp-content/uploads/2019/08/management-layer-blendr.io_.png title="management layer logilink.io" alt="management layer logilink.io">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-e13dd1d elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=e13dd1d data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-a704d14 elementor-column elementor-col-50 elementor-top-column" data-id=a704d14 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e0a0134 elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=e0a0134 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7683026 elementor-widget elementor-widget-text-editor" data-id=7683026 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>DASHBOARD – customise information dashboards for different user experiences</h2>
                                       <p>Easily manage your customers, integrations and the way you embed them into your SaaS platform. logilink.io is a flexible and powerful integration platform that solves the integration challenges for all types of customers.</p>
                                       <p>Use logilink.io to embed self-service &amp; high-volume integrations to your platform or build custom integrations for your enterprise-level customers with specific needs.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7eeea1d elementor-widget elementor-widget-button" data-id=7eeea1d data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-b56bcd7 elementor-widget elementor-widget-spacer" data-id=b56bcd7 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>









                        <section
               class="elementor-element elementor-element-1d516d5 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=1d516d5 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-1ca0ca3 elementor-column elementor-col-50 elementor-top-column" data-id=1ca0ca3 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-9e03c53 elementor-widget elementor-widget-spacer" data-id=9e03c53 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-ce42369 elementor-widget elementor-widget-text-editor" data-id=ce42369 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>NETWORK with your clients and suppliers and be able to view other global facilitators</h2>
                                       <p>logilink.io has a rapidly growing library of connected applications. Join the logilink.io ecosystem and integrate your platform with 500+ marketing, sales, and events cloud tools.</p>
                                       <p>Did not find the connector you need? The logilink.io team will add it within a couple of days.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-6f416a7 elementor-widget elementor-widget-button" data-id=6f416a7 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html target=_blank class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-1de3377 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=1de3377 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-7b025c1 elementor-column elementor-col-50 elementor-top-column" data-id=7b025c1 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-9b7e51f elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=9b7e51f data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-caed7a5 elementor-widget elementor-widget-image" data-id=caed7a5 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=index.html >
                                       <img
                                          src=wp-content/uploads/2019/08/blendr.io-300-connectors.png title="logilink.io 300 connectors" alt="logilink.io 300 connectors">    </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-d7b8b70 elementor-widget elementor-widget-text-editor" data-id=d7b8b70 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p><a
                                          name=blendeditor></a></p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-bf9e974 elementor-widget elementor-widget-spacer" data-id=bf9e974 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>






<section
               class="elementor-element elementor-element-ff79371 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=ff79371 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-f5dc0bb elementor-column elementor-col-50 elementor-top-column" data-id=f5dc0bb data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e9d4d5c elementor-widget elementor-widget-spacer" data-id=e9d4d5c data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-e7152bf elementor-widget elementor-widget-image" data-id=e7152bf data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=wp-content/uploads/2019/08/management-layer-blendr.io_.png title="management layer logilink.io" alt="management layer logilink.io">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-e13dd1d elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=e13dd1d data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-a704d14 elementor-column elementor-col-50 elementor-top-column" data-id=a704d14 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-e0a0134 elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=e0a0134 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7683026 elementor-widget elementor-widget-text-editor" data-id=7683026 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>MANAGE the dynamics of digital working </h2>
                                       <p>Easily manage your customers, integrations and the way you embed them into your SaaS platform. logilink.io is a flexible and powerful integration platform that solves the integration challenges for all types of customers.</p>
                                       <p>Use logilink.io to embed self-service &amp; high-volume integrations to your platform or build custom integrations for your enterprise-level customers with specific needs.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7eeea1d elementor-widget elementor-widget-button" data-id=7eeea1d data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-b56bcd7 elementor-widget elementor-widget-spacer" data-id=b56bcd7 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>









                        <section
               class="elementor-element elementor-element-1d516d5 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=1d516d5 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-1ca0ca3 elementor-column elementor-col-50 elementor-top-column" data-id=1ca0ca3 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-9e03c53 elementor-widget elementor-widget-spacer" data-id=9e03c53 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-ce42369 elementor-widget elementor-widget-text-editor" data-id=ce42369 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>EXPLORE  new services, revenue streams business models through digital eyes</h2>
                                       <p>logilink.io has a rapidly growing library of connected applications. Join the logilink.io ecosystem and integrate your platform with 500+ marketing, sales, and events cloud tools.</p>
                                       <p>Did not find the connector you need? The logilink.io team will add it within a couple of days.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-6f416a7 elementor-widget elementor-widget-button" data-id=6f416a7 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html target=_blank class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-1de3377 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=1de3377 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-7b025c1 elementor-column elementor-col-50 elementor-top-column" data-id=7b025c1 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-9b7e51f elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=9b7e51f data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-caed7a5 elementor-widget elementor-widget-image" data-id=caed7a5 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <a
                                          href=index.html >
                                       <img
                                          src=wp-content/uploads/2019/08/blendr.io-300-connectors.png title="logilink.io 300 connectors" alt="logilink.io 300 connectors">    </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-d7b8b70 elementor-widget elementor-widget-text-editor" data-id=d7b8b70 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <p><a
                                          name=blendeditor></a></p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-bf9e974 elementor-widget elementor-widget-spacer" data-id=bf9e974 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>




























            <section
               class="elementor-element elementor-element-8e19535 elementor-section-stretched elementor-hidden-desktop elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=8e19535 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-9fc550a elementor-column elementor-col-50 elementor-top-column" data-id=9fc550a data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-02fd9b6 elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=02fd9b6 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-094d672 elementor-widget elementor-widget-text-editor" data-id=094d672 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2>Management layer</h2>
                                       <p>Easily manage your customers, integrations and the way you embed them into your SaaS platform. logilink.io is a flexible and powerful integration platform that solves the integration challenges for all types of customers.</p>
                                       <p>Use logilink.io to embed self-service &amp; high-volume integrations to your platform or build custom integrations for your enterprise-level customers with specific needs.</p>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-40c7269 elementor-widget elementor-widget-button" data-id=40c7269 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Learn more</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-4b00260 elementor-widget elementor-widget-spacer" data-id=4b00260 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-c6f0321 elementor-column elementor-col-50 elementor-top-column" data-id=c6f0321 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-918bf55 elementor-widget elementor-widget-spacer" data-id=918bf55 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-b0fce5c elementor-widget elementor-widget-image" data-id=b0fce5c data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=wp-content/uploads/elementor/thumbs/management-layer-logilink.io_-obvkt6y6ljo2rew3ykum8kvpp0f5ox9nanuilczoms.png title="management layer logilink.io" alt="management layer logilink.io">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-80d0af4 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=80d0af4 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="custom-green-background elementor-element elementor-element-e0dcbfd elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=e0dcbfd data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;gradient&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-c77cd33 elementor-column elementor-col-100 elementor-top-column" data-id=c77cd33 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-6d01d09 elementor-widget elementor-widget-spacer" data-id=6d01d09 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-fb2bd0a elementor-widget elementor-widget-text-editor" data-id=fb2bd0a data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2 style="text-align: center;"><span
                                          style="color: #d81a5d;">logilink.io is an iPaaS (Integration Platform as a Service) solution for SaaS companies and agencies</span></h2>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-4ac39fb elementor-widget elementor-widget-spacer" data-id=4ac39fb data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="custom-green-background elementor-element elementor-element-cce8779 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=cce8779 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;gradient&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class alignment-wrap">
                     <div
                        class="elementor-element elementor-element-3cf48b7 elementor-column elementor-col-100 elementor-top-column" data-id=3cf48b7 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-6b43793 elementor-widget elementor-widget-image" data-id=6b43793 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/OPEN.png title=intuitive alt=intuitive>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-b1caddc elementor-widget elementor-widget-text-editor" data-id=b1caddc data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: #d81a5d;">OPEN</span></h3>
                                       <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">LogiLink is an open global logistics platform</span></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="elementor-element elementor-element-5b400f0 elementor-column elementor-col-100 elementor-top-column" data-id=5b400f0 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-22e6839 elementor-widget elementor-widget-image" data-id=22e6839 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Fast.png title="cloud-computing (1)" alt="cloud-computing (1)">
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-3f0f108 elementor-widget elementor-widget-text-editor" data-id=3f0f108 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color:#d81a5d;">FAST</span></h3>
                                       <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">Rapid deployment of apps for testing and integration</span></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="elementor-element elementor-element-525dc29 elementor-column elementor-col-100 elementor-top-column" data-id=525dc29 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-369bc8b elementor-widget elementor-widget-image" data-id=369bc8b data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/FLEXIBLE.png title=connectors alt=connectors>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-b448e3a elementor-widget elementor-widget-text-editor" data-id=b448e3a data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: #d81a5d;">FLEXIBLE</span></h3>
                                 <!--       <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">A rapidly growing library of 500+ connectors to various cloud applications&nbsp;</span></p> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="custom-green-background elementor-element elementor-element-89bb302 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=89bb302 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;gradient&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                 <div class="elementor-row custom-section-class alignment-wrap">
                     <div
                        class="elementor-element elementor-element-b22eec2 elementor-column elementor-col-100 elementor-top-column" data-id=b22eec2 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-f0c7b79 elementor-widget elementor-widget-image" data-id=f0c7b79 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Robust.png title=scalable alt=scalable>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-8a359d9 elementor-widget elementor-widget-text-editor" data-id=8a359d9 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: #d81a5d;">ROBUST</span></h3>
                               <!--         <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">Use the visual workflow builder to create integrations without coding and embed them inside your platform</span></p> -->
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-5c0407e elementor-widget elementor-widget-spacer" data-id=5c0407e data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-17c422d elementor-column elementor-col-100 elementor-top-column" data-id=17c422d data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-c4d0cab elementor-widget elementor-widget-image" data-id=c4d0cab data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/INTUITIVE.png title="cloud-computing (2)" alt="cloud-computing (2)">
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-bbf1e2d elementor-widget elementor-widget-text-editor" data-id=bbf1e2d data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: #d81a5d;">INTUITIVE and ease of navigation</span></h3>
                                      <!--  <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">logilink.io supports big data as well as hyper-scalable and distributed execution to manage billions of data records</span></p> -->
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-7d074c6 elementor-widget elementor-widget-spacer" data-id=7d074c6 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div
                        class="elementor-element elementor-element-0021c4f elementor-column elementor-col-100 elementor-top-column" data-id=0021c4f data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-f8a6028 elementor-widget elementor-widget-image" data-id=f8a6028 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/SCALABLE.png title=secure-shield alt=secure-shield>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-4029117 elementor-widget elementor-widget-text-editor" data-id=4029117 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color:#d81a5d;">SCALABLE</span></h3>
                                <!--        <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">logilink.io is a hyper-secure platform with 2-factor authentication and external testing and audits</span></p> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>



                        <section class="custom-green-background elementor-element elementor-element-cce8779 elementor-section-stretched elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=cce8779 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;gradient&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-3cf48b7 elementor-column elementor-col-100 elementor-top-column" data-id=3cf48b7 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-6b43793 elementor-widget elementor-widget-image" data-id=6b43793 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/secure.png title=intuitive alt=intuitive>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-b1caddc elementor-widget elementor-widget-text-editor" data-id=b1caddc data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: #d81a5d;">SECURE</span></h3>
                                     <!--   <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">Build customized enterprise-grade integrations as well as standardized self-service integrations</span></p> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="elementor-element elementor-element-5b400f0 elementor-column elementor-col-100 elementor-top-column" data-id=5b400f0 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-22e6839 elementor-widget elementor-widget-image" data-id=22e6839 data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-image>
                                       <img
                                          src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Global.png title="cloud-computing (1)" alt="cloud-computing (1)">
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-3f0f108 elementor-widget elementor-widget-text-editor" data-id=3f0f108 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color:#d81a5d;">GLOBAL</span></h3>
                                <!--        <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">Powerful integration features including filters, variables, conditions, data queues, webhook support, schedules and more</span></p> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="elementor-element elementor-element-525dc29 elementor-column elementor-col-100 elementor-top-column" data-id=525dc29 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                              <div
                                 class="elementor-element elementor-element-369bc8b elementor-widget elementor-widget-image" data-id=369bc8b data-element_type=widget data-widget_type=image.default>
                                 <div
                                    class=elementor-widget-container>
                                    <!-- <div
                                       class=elementor-image>
                                       <img
                                          src=wp-content/uploads/elementor/thumbs/connectors-o6g8pgoru3oawotjpvobct8er94wnmwj2iiifc0qh0.png title=connectors alt=connectors>
                                    </div> -->
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-b448e3a elementor-widget elementor-widget-text-editor" data-id=b448e3a data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                   <!-- <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h3 style="text-align: center;"><span
                                          style="color: #d81a5d;">GLOBAL</span></h3>
                                        <p
                                          style="text-align: center;"><span
                                          style="color: #ffffff; font-size: 12pt;">A rapidly growing library of 500+ connectors to various cloud applications&nbsp;</span></p>
                                    </div>-->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>




            <section class="custom-grey-background elementor-element elementor-element-b9ad3b3 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id=b9ad3b3 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-default">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-67141b7 elementor-column elementor-col-100 elementor-top-column" data-id=67141b7 data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                            <!--   <div
                                 class="elementor-element elementor-element-f561b85 elementor-widget elementor-widget-spacer" data-id=f561b85 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div> -->
                              <div
                                 class="elementor-element elementor-element-81fe7a2 elementor-widget elementor-widget-text-editor" data-id=81fe7a2 data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2 style="text-align: center;">Interested to learn more about the logilink.io integration platform?</h2>
                                    </div>
                                 </div>
                              </div>
                              <!-- <div
                                 class="elementor-element elementor-element-529aaf2 elementor-widget elementor-widget-spacer" data-id=529aaf2 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div> -->
                              <div
                                 class="elementor-element elementor-element-057bdc5 elementor-align-center elementor-widget elementor-widget-button" data-id=057bdc5 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-md" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>Contact us</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                             <!--  <div
                                 class="elementor-element elementor-element-58b3a85 elementor-widget elementor-widget-spacer" data-id=58b3a85 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="white-background elementor-element elementor-element-fa98a82 elementor-section-stretched elementor-section-height-min-height elementor-section-content-top elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id=fa98a82 data-element_type=section data-settings={&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}>
               <div
                  class="elementor-container elementor-column-gap-wider">
                  <div class="elementor-row custom-section-class">
                     <div
                        class="elementor-element elementor-element-8cb859e elementor-column elementor-col-100 elementor-top-column" data-id=8cb859e data-element_type=column>
                        <div
                           class="elementor-column-wrap  elementor-element-populated">
                           <div
                              class=elementor-widget-wrap>
                             <!--  <div
                                 class="elementor-element elementor-element-2f86d4b elementor-widget elementor-widget-spacer" data-id=2f86d4b data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div> -->
                              <div
                                 class="elementor-element elementor-element-5a7195f elementor-widget elementor-widget-text-editor" data-id=5a7195f data-element_type=widget data-widget_type=text-editor.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-text-editor elementor-clearfix">
                                       <h2 style="text-align: center;"><span
                                          style="color: #d81a5d;">Discover the latest blog articles</span></h2>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-d8b7755 elementor-widget elementor-widget-spacer" data-id=d8b7755 data-element_type=widget data-widget_type=spacer.default>
                                <!--  <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div> -->
                              </div>
                              <div
                                 class="elementor-element elementor-element-621277c elementor-grid-3 elementor-grid-tablet-2 elementor-grid-mobile-1 elementor-posts--thumbnail-top elementor-card-shadow-yes elementor-posts__hover-gradient elementor-widget elementor-widget-posts" data-id=621277c data-element_type=widget data-settings={&quot;cards_columns&quot;:&quot;3&quot;,&quot;cards_columns_tablet&quot;:&quot;2&quot;,&quot;cards_columns_mobile&quot;:&quot;1&quot;,&quot;cards_row_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:35,&quot;sizes&quot;:[]}} data-widget_type=posts.cards>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class="elementor-posts-container elementor-posts elementor-posts--skin-cards elementor-grid">

                                       <?php
                     $q= array('post_type'=>'post','posts_per_page'=>3,'post_status'=>'publish');
                     $loop = new WP_Query($q);
                     if ( $loop->have_posts() ) : while ( $loop->have_posts() )  : $loop->the_post(); 

                        $id = get_the_ID();
                        $terms = get_the_terms($id, 'connector_category' );
                        
                     ?>
               
                      
                      <article
                                          class="elementor-post elementor-grid-item post-7374 post type-post status-publish format-standard has-post-thumbnail hentry category-blog tag-cloud-communications tag-ipaas tag-ucaas">
                                          <div
                                             class=elementor-post__card>
                                             <a
                                                class=elementor-post__thumbnail__link href=index.html >
                                                <div
                                                   class=elementor-post__thumbnail>
                                                   <!-- <img
                                                      width=1200 height=630 src=client/q_glossy_ret_img_w_1200/wp-content/uploads/2020/05/integrationusecase-synccalltranscriptions_38da9613e2824020195d41bfca24c16a_2000.jpg class="attachment-full size-full" alt="Integration use case:  sync call transcriptions" srcset="client/q_glossy_ret_img_w_1200/wp-content/uploads/2020/05/integrationusecase-synccalltranscriptions_38da9613e2824020195d41bfca24c16a_2000.jpg 1200w, client/q_glossy_ret_img_w_300/wp-content/uploads/2020/05/integrationusecase-synccalltranscriptions_38da9613e2824020195d41bfca24c16a_2000-300x158.jpg 300w, client/q_glossy_ret_img_w_1024/wp-content/uploads/2020/05/integrationusecase-synccalltranscriptions_38da9613e2824020195d41bfca24c16a_2000-1024x538.jpg 1024w, client/q_glossy_ret_img_w_768/wp-content/uploads/2020/05/integrationusecase-synccalltranscriptions_38da9613e2824020195d41bfca24c16a_2000-768x403.jpg 768w" sizes="(max-width: 1200px) 100vw, 1200px"> -->
                                                   <!-- <img src=wp-content/uploads/2020/05/Integrationusecase-synccalltranscriptions_38da9613e2824020195d41bfca24c16a_2000%20(1).jpg title=cloud-communications-integrations alt=cloud-communications-integrations> -->
                                                   <?php echo the_post_thumbnail(); ?> 
                                                </div>
                                             </a>
                                             <div
                                                class=elementor-post__text>
                                                <h3 class="elementor-post__title">
                                                   <a
                                                      href="<?php echo get_post_permalink(); ?>">
                                                   <?php echo the_title(); ?> </a>
                                                </h3>
                                                <a
                                                   class=elementor-post__read-more href="<?php echo get_post_permalink(); ?>" >
                                                Read More » </a>
                                             </div>
                                          </div>
                                       </article>
                      
                     <?php
                        endwhile;
                        endif;
                     ?>



                                       





                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-6b3abda elementor-widget elementor-widget-spacer" data-id=6b3abda data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-0589561 elementor-align-center elementor-widget elementor-widget-button" data-id=0589561 data-element_type=widget data-widget_type=button.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-button-wrapper>
                                       <a
                                          href=index.html class="elementor-button-link elementor-button elementor-size-sm" role=button>
                                       <span
                                          class=elementor-button-content-wrapper>
                                       <span
                                          class=elementor-button-text>More articles</span>
                                       </span>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="elementor-element elementor-element-63e5741 elementor-widget elementor-widget-spacer" data-id=63e5741 data-element_type=widget data-widget_type=spacer.default>
                                 <div
                                    class=elementor-widget-container>
                                    <div
                                       class=elementor-spacer>
                                       <div
                                          class=elementor-spacer-inner></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
<?php
   get_footer();
   ?>