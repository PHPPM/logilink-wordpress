<?php
/*
*
Template Name: Connectors template
*
*/
get_header();
?>

<!doctype html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <!-- CSRF Token -->
	    <meta name="csrf-token" content="KZFtwkRnNeEi3TWd7awYoF9bfPcJwzkWgalkE8eI">
	  <!--   <link href="9fd7d92b7479/css/app.css" rel="stylesheet"> -->
	    <link rel="apple-touch-icon" sizes="180x180" href="logo/favicons/d755e2947dd62f18b748530f96d3c1aa/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="16x16" href="logo/favicons/d755e2947dd62f18b748530f96d3c1aa/favicon-16x16.png">
		<link rel="icon" type="image/png" sizes="32x32" href="logo/favicons/d755e2947dd62f18b748530f96d3c1aa/favicon-32x32.png">
		<link rel="icon" type="image/png" href="logo/favicons/d755e2947dd62f18b748530f96d3c1aa/favicon-32x32.png">

		<link rel=stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	    <link rel=stylesheet href="<?php echo site_url(); ?>/wp-content/themes/blend/css/custom.css">


	             
	    <title>
			Integrations - Logicon systems
	    </title>
	</head>
<body>
<div id="app" :style="style">
	<header class="header">
		<div class="header__content" :class="headerContentClass">
		<div>
			<a href="<?php echo site_url(); ?>">
				<img class="header__logo" src="http://49.249.236.30:2728/logi/wp-content/uploads/2020/07/02e54f9a7dbe68d975976413f7b329aa.png" alt="Blendr.io">
			</a>
		</div>
		<div class="nav-bar">
			<div>
				<a href="www_subdomain/getting-started.html" target="_blank" rel="noopener">
					<i aria-hidden="true" class="fa fa-question-circle" style="font-size: 1.5em"></i>
				</a>
			</div>
			<div class="nav-menu-container" v-click-outside="hideHeaderNavbar">
				<a href="#" @click.prevent="headerNavbar = !headerNavbar">
					<i class="fa fa-caret-down" style="font-size: 1.9em" aria-hidden="true"></i>
				</a>
				<div class="nav-menu" style="display: none;" v-show="headerNavbar" v-cloak @click="hideHeaderNavbar">
					<a href="login.html">Sign in <i class="fa fa-user" aria-hidden="true"></i></a>
				</div>
			</div>
			</div>
		</div>
	</header>
	<main>
		<div class="main-content main-content-full-width">
			<section class="datasourceintegration-hero">
			<div class="container-medium">
			<div class="help-paragraph" >
				<?php
				while ( have_posts() ) : the_post();color: #333;
					echo get_the_content();
				endwhile;
				?>
			<!-- <h2>Blendr.io is an embedded integration and automation platform for SaaS companies</h2>
			<p>Integrate your SaaS Platform with 500+ marketing, sales and event cloud tools</p>
			<p>Did not find a cloud tool? Do you want to add your platform to the list of the Blendr.io connectors? <a href="#" style="text-decoration: underline">Let us know!</a></p>
			 -->
			</div>
			</div>
			</section>

				<div class="" id="filter-tabs">
					<div class="left-side">
						
						<div class="all-categories">
							<h4 id="chooseTopics">
						        Choose Topics
						    </h4>
							<ul>
								<li class="all active">All categories</li>

								<?php
								   $args = array(
							               'taxonomy' => 'connector_category',
							               'orderby' => 'name',
							               'order'   => 'ASC'
								           );
								   $cats = get_categories($args);
								   foreach($cats as $cat) {
								?>
								    <li class="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></li>
								<?php
								   }
								?>

							</ul>
						</div>


					</div>




					<div class="right-side">
						<!-- <div class="search-control"><input type="search" placeholder="Search" class=""> <i class="fa fa-search"></i></div> -->
						<ul id="myList" class="right-side" data-ids = "24" >
							<?php
							$q= array('post_type'=>'connector','posts_per_page'=>-1,'post_status'=>'publish');
							$loop = new WP_Query($q);
							if ( $loop->have_posts() ) : while ( $loop->have_posts() )  : $loop->the_post(); 

								$id = get_the_ID();
							 	$terms = get_the_terms($id, 'connector_category' );
							 	
							?>
							<li style="display:none;" class="card <?php foreach ($terms as $term) {  echo $term->slug.' ';  }?>">
								<div class="card all <?php foreach ($terms as $term) {  echo $term->slug.' ';  }?>" >
									<a href="<?php echo get_post_permalink(); ?>" class="content">
										<?php echo the_post_thumbnail(); ?> 
										<div class="title"><?php echo the_title(); ?></div> 
										<!----><br> <!----> <!---->
									 </a>
								</div>
							</li>
							 
							 
							 
							<?php
								endwhile;
							 	endif;
							?>
						</ul>

					</div>	
				</div>





			<section>
			<div class="container-medium datasourceintegration-detail mt-4 p-5">

			</div>
			</section>

			<footer class="datasourceintegration-footer">
			<h3 class="mt-3 mb-3">Use Logicon systems to add comprehensive integrations to your SaaS platform</h3>
			<a href="www_subdomain/index.html" class="btn btn-default" style="background: #fff;">More about Logicon systems</a>
			</footer>
		</div>
	</main>
 
    <!-- Scripts -->

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function(){

				$("li").click(function() {
				   var colorClass = this.className;
				   //alert(colorClass);
				   $('li').removeClass('active');
				   $(this).addClass('active');
				   $('.card').hide();
				   $('.card.'+colorClass).show();
				   $('li.card.'+colorClass).show();
				});
				$(".nav-menu-container a").click(function(){
					$(".nav-menu-container").toggleClass("active");
				})
			});

			//$('#myList li:lt('+30+')').show();
			// $(window).scroll(function() {
	  //  			if($(window).scrollTop() == $(document).height() - $(window).height() - 250) {
			// 	    var size_li = $( "#myList li" ).length;
			// 	    var x = $("#myList").attr('data-ids');
			// 	  	var c = x;
			// 	    x= (x+6 <= size_li) ? x+6 : size_li;
			// 	    $('#myList li:lt('+x+')').show();
			// 	    $("#myList").attr('data-ids','');
			// 	    $("#myList").attr('data-ids',x);

			//     }
			// });

			 var xx = $("#myList").attr('data-ids');
			 $('#myList li:lt('+xx+')').show();

			$(document).scroll(function () {
			    var y = $(this).scrollTop();
			    //alert(y);
			    if ($(window). scrollTop() + $(window). height() > $(document). height() - 1500)  {
			        var size_li = $( "#myList li" ).length;
				    var x = $("#myList").attr('data-ids');
				    x = parseInt(x) + 3;

				    //alert(x);
				    $('#myList li:lt('+x+')').show();
				    $("#myList").attr('data-ids','');
				    $("#myList").attr('data-ids',x);
			    } else {
			        //$('.bottomMenu').fadeOut();
			    }

			});






		</script>

    </body>
</html>



<?php
//get_footer();

?>