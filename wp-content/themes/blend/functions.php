<?php
/**
 * Blend functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Blend
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'blend_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function blend_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Blend, use a find and replace
		 * to change 'blend' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'blend', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'blend' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'blend_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'blend_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function blend_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'blend_content_width', 640 );
}
add_action( 'after_setup_theme', 'blend_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function blend_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'blend' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'blend' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'blend_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function blend_scripts() {
	wp_enqueue_style( 'blend-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'blend-style', 'rtl', 'replace' );

	wp_enqueue_script( 'blend-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'blend_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/*Custom Post type : Connectors*/


$portpost = array(
    'name' => _x('connector', 'post type general name', 'devwp'),
    'singular_name' => _x('connectors', 'post type singular name', 'devwp'),
    'add_new' => _x('Add New connector', 'connector', 'devwp'),
    'add_new_item' => __('Add New', 'devwp'),
    'edit_item' => __('Edit connector', 'devwp'),
    'new_item' => __('New connector', 'devwp'),
    'all_items' => __('All connector', 'devwp'),
    'view_item' => __('View connector', 'devwp'),
    'search_items' => __('Search  connector', 'devwp'),
    'not_found' =>  __('No connector connector', 'devwp'),
    'not_found_in_trash' => __('No connector found in Trash', 'devwp'), 
    'parent_item_colon' => '',
    'menu_name' => __('Connector', 'devwp')

  );
  $args = array(
    'labels' => $portpost,
    'public' => false,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => _x( 'connector', 'URL slug', 'devwp' ) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-format-gallery',
    'supports' => array( 'title','editor','thumbnail', 'page-attributes','custom-fields'),
    'taxonomies' => array('connector_category')
  ); 
  register_post_type('connector', $args);
  register_taxonomy('connector_category', 'connector', array('hierarchical' => true, 'label' => 'connector Category', 'singular_name' => 'connector Category','show_admin_column' => true, "rewrite" => true, "query_var" => true));



/*Custom post Company*/


$portpost = array(
    'name' => _x('company', 'post type general name', 'devwp'),
    'singular_name' => _x('company', 'post type singular name', 'devwp'),
    'add_new' => _x('Add New company', 'company', 'devwp'),
    'add_new_item' => __('Add New', 'devwp'),
    'edit_item' => __('Edit company', 'devwp'),
    'new_item' => __('New company', 'devwp'),
    'all_items' => __('All company', 'devwp'),
    'view_item' => __('View company', 'devwp'),
    'search_items' => __('Search  company', 'devwp'),
    'not_found' =>  __('No company company', 'devwp'),
    'not_found_in_trash' => __('No company found in Trash', 'devwp'), 
    'parent_item_colon' => '',
    'menu_name' => __('Company', 'devwp')

  );
  $args = array(
    'labels' => $portpost,
    'public' => false,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => _x( 'company', 'URL slug', 'devwp' ) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-format-gallery',
    'supports' => array( 'title','editor','thumbnail', 'page-attributes','custom-fields'),
    'taxonomies' => array('company_category')
  ); 
  register_post_type('company', $args);
  register_taxonomy('company_category', 'company', array('hierarchical' => true, 'label' => 'company Category', 'singular_name' => 'company Category','show_admin_column' => true, "rewrite" => true, "query_var" => true));




  /*custom post services*/

  $portpost = array(
    'name' => _x('service', 'post type general name', 'devwp'),
    'singular_name' => _x('service', 'post type singular name', 'devwp'),
    'add_new' => _x('Add New service', 'service', 'devwp'),
    'add_new_item' => __('Add New', 'devwp'),
    'edit_item' => __('Edit service', 'devwp'),
    'new_item' => __('New service', 'devwp'),
    'all_items' => __('All service', 'devwp'),
    'view_item' => __('View service', 'devwp'),
    'search_items' => __('Search  service', 'devwp'),
    'not_found' =>  __('No service service', 'devwp'),
    'not_found_in_trash' => __('No service found in Trash', 'devwp'), 
    'parent_item_colon' => '',
    'menu_name' => __('Service', 'devwp')

  );
  $args = array(
    'labels' => $portpost,
    'public' => false,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => _x( 'service', 'URL slug', 'devwp' ) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-format-gallery',
    'supports' => array( 'title','editor','thumbnail', 'page-attributes','custom-fields'),
    'taxonomies' => array('service_category')
  ); 
  register_post_type('service', $args);
  register_taxonomy('service_category', 'service', array('hierarchical' => true, 'label' => 'service Category', 'singular_name' => 'service Category','show_admin_column' => true, "rewrite" => true, "query_var" => true));
