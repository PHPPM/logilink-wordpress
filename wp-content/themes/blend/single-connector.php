<?php

get_header();

?>



<!doctype html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <!-- CSRF Token -->
	    <meta name="csrf-token" content="KZFtwkRnNeEi3TWd7awYoF9bfPcJwzkWgalkE8eI">
	    <link href="9fd7d92b7479/css/app.css" rel="stylesheet">
	    <link rel="apple-touch-icon" sizes="180x180" href="logo/favicons/d755e2947dd62f18b748530f96d3c1aa/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="16x16" href="logo/favicons/d755e2947dd62f18b748530f96d3c1aa/favicon-16x16.png">
		<link rel="icon" type="image/png" sizes="32x32" href="logo/favicons/d755e2947dd62f18b748530f96d3c1aa/favicon-32x32.png">
		<link rel="icon" type="image/png" href="logo/favicons/d755e2947dd62f18b748530f96d3c1aa/favicon-32x32.png">

		<link rel=stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	    <link rel=stylesheet href="<?php echo site_url(); ?>/wp-content/themes/blend/css/custom.css">


	             
	    <title>
			Integrations - Logicon systems
	    </title>
	</head>
<body class="page-connector-detail">
<div id="app" :style="style">
	<header class="header">
		<div class="header__content" :class="headerContentClass">
		<div>
			<a href="<?php echo site_url(); ?>">
				<img class="header__logo" src="http://49.249.236.30:2728/logi/wp-content/uploads/2020/07/02e54f9a7dbe68d975976413f7b329aa.png" alt="Blendr.io">
			</a>
		</div>
		<div class="nav-bar">
			<div>
				<a href="www_subdomain/getting-started.html" target="_blank" rel="noopener">
					<i aria-hidden="true" class="fa fa-question-circle" style="font-size: 1.5em"></i>
				</a>
			</div>
			<div class="nav-menu-container" v-click-outside="hideHeaderNavbar">
				<a href="#" @click.prevent="headerNavbar = !headerNavbar">
					<i class="fa fa-caret-down" style="font-size: 1.9em" aria-hidden="true"></i>
				</a>
				<div class="nav-menu" style="display: none;" v-show="headerNavbar" v-cloak @click="hideHeaderNavbar">
					<a href="login.html">Sign in <i class="fa fa-user" aria-hidden="true"></i></a>
				</div>
			</div>
			</div>
		</div>
	</header>
	<main>
		<div class="main-content main-content-full-width">
			<section class="datasourceintegration-hero">
			<div class="container-medium">
			<div class="help-paragraph">
				
				<div class="row">
					<div class="col-md-6">

						<?php
							while ( have_posts() ) : the_post();
								echo"<h1 class='detail-page-connector-title-hai-yeh'>";
								echo get_the_title(); 
								echo"</h1>";
								echo"<p class='detail-page-connector-detail-hai-yeh'>";
								echo get_the_content();
								echo"</p>";
							endwhile;
						?>
					
					</div>
				 <div class="col-md-6"><div class="inner-page-banner"><?php echo the_post_thumbnail(); ?></div></div></div>
			<!-- <h2>Blendr.io is an embedded integration and automation platform for SaaS companies</h2>
			<p>Integrate your SaaS Platform with 500+ marketing, sales and event cloud tools</p>
			<p>Did not find a cloud tool? Do you want to add your platform to the list of the Blendr.io connectors? <a href="#" style="text-decoration: underline">Let us know!</a></p>
			 -->
			</div>
			</div>
			</section>

			<?php   
			$data = get_field('connector_detail_title');
			if($data == ""){ 
			}else{ ?>
				
				<section class="integrations">
					<div class="container-medium">
						<div class="help-paragraph">
							<div class="row">
								<div class="datasourceintegration-detail-connector">
									<div>
										<div class="flex-row">color: #333;
											<div class="blend detail description">
												<h2 style="text-align: left;"><?php the_field('connector_detail_title'); ?></h2> 
												<div class="help-paragraph mt-4 mr-4">
													<?php the_field('connector_detail_description'); ?>
												</div> 
												<div class="mt-5">
													<a href="#" class="main-button">Contact
													Us</a> 
													<a href="/logi/connects/" class="btn btn-default btn-lg">All Connectors</a>
												</div>
											</div> 
											<img src="<?php the_field('connector_detail_description_image'); ?>" class="ml-4" style="width: 520px;">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>

			<?php }

			?>
			




<?php if( have_rows('connector_features_list') ): ?>
<section class="supported-features">
	<div class="container-medium">
			<div class="mt-5"><h2><?php echo the_field('section_label'); ?></h2></div>

			<div class="container-cards container-cards-2 container-endpoints">
		
				
				    <?php while( have_rows('connector_features_list') ): the_row(); 
				        $image = get_sub_field('features_title');
				        ?>
					<div class="card no-hover">
						<div>
							<span aria-hidden="true" class="glyphicon glyphicon-folder-close"></span>
						</div>
						<div>
							<h5 class="title"> <?php echo $image; ?></h5> 
							<div class="description"></div>
						</div>
					</div> 
				    <?php endwhile; ?>
	

			</div>


            <div style="text-align: center;"><a href="<?php echo the_field('reference_link'); ?>" class="main-button" target="_blank">Learn more</a></div>
        </div>
        </section><section class="integrations">
					<div class="container-medium">
						<div class="help-paragraph">
							<div class="row">
								<div class="datasourceintegration-detail-connector">
									<div>
										<div class="flex-row">
											<div class="blend detail description">
												<h2 style="text-align: left;"><?php the_field('connector_detail_title'); ?></h2> 
												<div class="help-paragraph mt-4 mr-4">
													<?php the_field('connector_detail_description'); ?>
												</div> 
												<div class="mt-5">
													<a href="#" class="main-button">Contact
													Us</a> 
													<a href="/logi/connects/" class="btn btn-default btn-lg">All Connectors</a>
												</div>
											</div> 
											<img src="<?php the_field('connector_detail_description_image'); ?>" class="ml-4" style="width: 520px;">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>

			<?php endif; ?>
			
			<div class="heading-section-blends-list">
				<h2>Integrate your SaaS platform with 500+ cloud tools</h2>
			</div>

				<div class="" id="filter-tabs">

				



					<div class="right-side">

						<?php
						$q= array('post_type'=>'connector','posts_per_page'=>10,'post_status'=>'publish');
						$loop = new WP_Query($q);
						if ( $loop->have_posts() ) : while ( $loop->have_posts() )  : $loop->the_post(); 

							$id = get_the_ID();
						 	$terms = get_the_terms($id, 'connector_category' );
						 	
						?>
						<div class="card all <?php foreach ($terms as $term) {  echo $term->slug.' ';  }?>" >
							<a href="<?php echo get_post_permalink(); ?>" class="content">
								<?php echo the_post_thumbnail(); ?> 
								<div class="title"><?php echo the_title(); ?></div> 
								<!----><br> <!----> <!---->
							 </a>
						</div>
						 
						 
						 
						<?php
							endwhile;
						 	endif;
						?>

					</div>	
				</div>





			<!-- <section>
			<div class="container-medium datasourceintegration-detail mt-4 p-5">

			</div>
			</section> -->

			<footer class="datasourceintegration-footer">
			<h3 class="mt-3 mb-3">Use Logicon systems to add comprehensive integrations to your SaaS platform</h3>
			<a href="www_subdomain/index.html" class="btn btn-default" style="background: #fff;">More about Logicon systems</a>
			</footer>
		</div>
	</main>
 
    <!-- Scripts -->

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function(){

				$("li").click(function() {
				   var colorClass = this.className;

				   $('li').removeClass('active');
				   
				   $(this).addClass('active');
				   
				   $('.card').hide();
				   $('.card.'+colorClass).show();

				});
				$(".nav-menu-container a").click(function(){
					$(".nav-menu-container").toggleClass("active");
				})
			});
		</script>

    </body>
</html>



<?php
//get_footer();

?>