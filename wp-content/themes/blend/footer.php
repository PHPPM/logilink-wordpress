<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blend
 */

?>

	<footer><hr
class=footer-hr><div
class=container><div>
<img
src="http://49.249.236.30:2728/logi/wp-content/uploads/2020/10/Logicon_Systems_Logo.png" class="footer-logo"></div><div><ul
class=footer-menu><li
id=menu-item-631 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-631"><a
href=#>Product</a><ul
class=sub-menu><li
id=menu-item-5281 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5281"><a
href=integration-platform.html >Overview</a></li><li
id=menu-item-7503 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7503"><a
href=index.html >Compliance &#038; data security</a></li><li
id=menu-item-7367 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7367"><a
href=api-connectors.html >Connectivity layer</a></li><li
id=menu-item-5283 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5283"><a
href=integration-builder.html >Integration Layer</a></li><li
id=menu-item-5284 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5284"><a
href=embed-integrations.html >UI &#038; embedding Layer</a></li><li
id=menu-item-5285 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5285"><a
href=manage-integrations.html >Management Layer</a></li><li
id=menu-item-3162 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3162"><a
href=index.html >logilink.io versus Zapier</a></li><li
id=menu-item-5291 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5291"><a
href=index.html>Connectors</a></li></ul></li><li
id=menu-item-5286 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-5286"><a
href=#>Solutions</a><ul
class=sub-menu><li
id=menu-item-5287 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5287"><a
href=event-technology-integration.html >Event Tech</a></li><li
id=menu-item-5288 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5288"><a
href=integrate-saas-platform.html >Martech</a></li><li
id=menu-item-5290 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5290"><a
href=integrate-voip.html >Cloud Communications</a></li><li
id=menu-item-6524 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6524"><a
href=ai-saas-integrations.html >AI platforms</a></li><li
id=menu-item-7304 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7304"><a
href=index.html >Accounting Software</a></li><li
id=menu-item-7447 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7447"><a
href=index.html >HRM Platforms</a></li><li
id=menu-item-5289 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5289"><a
href=index.html >Other B2B SaaS</a></li><li
id=menu-item-6598 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6598"><a
href=index.html >Integrators &#038; Agencies</a></li><li
id=menu-item-5388 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5388"><a
href=index.html >Pricing</a></li><li
id=menu-item-5389 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5389"><a
href=index.html >logilink.io Startup Program</a></li></ul></li><li
id=menu-item-629 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-629"><a
href=#>Company</a><ul
class=sub-menu><li
id=menu-item-6280 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6280"><a
href=index.html >Webinar</a></li><li
id=menu-item-6909 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6909"><a
href=index.html >Training and certification</a></li><li
id=menu-item-5847 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5847"><a
href=index.html >Newsletter</a></li><li
id=menu-item-4697 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4697"><a
href=index.html >Careers</a></li><li
id=menu-item-762 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-762"><a
href=index.html >Blog</a></li><li
id=menu-item-689 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-689"><a
href=index.html >Privacy</a></li><li
id=menu-item-683 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-683"><a
href=index.html >Terms of use</a></li></ul></li><li
id=menu-item-638 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-638"><a
href=#>Help</a><ul
class=sub-menu><li
id=menu-item-4810 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4810"><a
href=https://www.logilink.io/contact/ >Contact</a></li><li
id=menu-item-7153 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7153"><a
href=index.html >Services</a></li><li
id=menu-item-2514 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2514"><a
href=index.html>Getting started</a></li><li
id=menu-item-5883 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5883"><a
href=index.html >Developers</a></li><li
id=menu-item-1073 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1073"><a
href=index.html >Help center</a></li><li
id=menu-item-5739 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5739"><a
href=index.html >The Ultimate API Checklist</a></li><li
id=menu-item-5740 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5740"><a
href=index.html >Types of integration embedding</a></li><li
id=menu-item-5741 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5741"><a
href=index.html >iPaaS service models</a></li></ul></li><li
id=menu-item-6449 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6449"><a
href=#>Case studies</a><ul
class=sub-menu><li
id=menu-item-6448 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6448"><a
href=index.html >Event Registration to CRM</a></li><li
id=menu-item-6502 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6502"><a
href=index.html >AI platform taps into new datasources</a></li><li
id=menu-item-6503 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6503"><a
href=index.html >AI platform adds native integrations</a></li></ul></li><!--<li>
<a
href=#>Follow us</a><ul
class=horizontal><li>
<a
href=https://www.facebook.com/blendrHQ/ target=_blank>
<i class="fab fa-facebook-f"></i>
</a></li><li>
<a
href=https://www.youtube.com/channel/UCZc1MDI2EHIFXZw8xYQxMfw target=_blank>
<i
aria-hidden=true class="fa fa-youtube fa-2x"></i>
</a></li><li>
<a
href=https://twitter.com/blendr_HQ target=_blank>
<i
aria-hidden=true class="fa fa-twitter fa-2x"></i>
</a></li><li>
<a
href=https://www.linkedin.com/company/blendr-io/ target=_blank>
<i
aria-hidden=true class="fa fa-linkedin fa-2x"></i>
</a></li><li>
<a
href=mailto:hello@logilink.io>
<i
aria-hidden=true class="fa fa-envelope fa-2x"></i>
</a></li></ul></li>--></ul></div></div><hr
class=footer-hr><div
class="footer-container copyright">
&copy; 2017 - 2020 logilink.io</div></footer>  </body></html>

<?php wp_footer(); ?>

</body>
</html>
