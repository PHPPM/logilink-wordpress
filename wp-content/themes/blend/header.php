<?php
   /**
    * The header for our theme
    *
    * This is the template that displays all of the <head> section and everything up until <div id="content">
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package Blend
    */
   
   ?>
<!DOCTYPE html>
<html lang=en>
   <head>
      <meta charset=utf-8>
      <meta http-equiv=X-UA-Compatible content="IE=edge">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <meta name=description content="Blendr.io - embedded integration platform for SaaS - iPaaS for native integrations">
      <script data-cfasync="false" id="ao_optimized_gfonts_config">WebFontConfig={google:{families:["Roboto:300,400,500,700,900","Roboto:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Roboto Slab:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"] },classes:false, events:false, timeout:1500};</script>
      <link href=<?php echo site_url(); ?>/wp-content/themes/blend/img/blendricon.png rel="shortcut icon" type=image/png>
      <!-- <link rel=stylesheet href=ajax/libs/jqueryui/1-12-1/themes/smoothness/jquery-ui.css> -->
      <meta name=theme-color content=#337ab7>
      <link media=all href=<?php echo site_url(); ?>/wp-content/cache/autoptimize/css/autoptimize_068f03581410086a86e150ef68588262.css rel=stylesheet>
      <title>iPaaS (integration platform as a service) for SaaS companies - Blendr.io</title>
      <meta name=description content="Create integrations with the low-code visual builder, embed them into the UI of your platform and centrally manage your integrations. iPaaS for SaaS.">
      <meta
         name=robots content="index, follow">
      <meta
         name=googlebot content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
      <meta
         name=bingbot content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
      <link
         rel=canonical href=https://www.blendr.io/ >
      <meta
         property=og:locale content=en_US>
      <meta
         property=og:type content=website>
      <meta
         property=og:title content="iPaaS (integration platform as a service) for SaaS companies - Blendr.io">
      <meta
         property=og:description content="Create integrations with the low-code visual builder, embed them into the UI of your platform and centrally manage your integrations. iPaaS for SaaS.">
      <meta
         property=og:url content=https://www.blendr.io/ >
      <meta
         property=og:site_name content=blendr.io>
      <meta
         property=article:modified_time content=2020-05-29T09:52:30+00:00>
      <meta
         property=og:image content=<?php echo site_url(); ?>/wp-content/uploads/2019/07/blendr.io-ipaas-for-saas.jpg>
      <meta
         property=og:image:width content=1200>
      <meta
         property=og:image:height content=630>
      <meta
         name=twitter:card content=summary>
      <meta
         name=twitter:title content="iPaaS (integration platform as a service) for SaaS companies - Blendr.io">
      <meta
         name=twitter:description content="Create integrations with the low-code visual builder, embed them into the UI of your platform and centrally manage your integrations. iPaaS for SaaS.">
      <meta name=twitter:image content=<?php echo site_url(); ?>/wp-content/uploads/2019/07/blendr.io-ipaas-for-saas.jpg>
      <link rel=dns-prefetch href=//s.w.org>
      <link href=https://cdn.shortpixel.ai rel=preconnect>
      <link href=https://fonts.gstatic.com crossorigin=anonymous rel=preconnect>
      <link href=https://ajax.googleapis.com rel=preconnect>
      <link href=https://fonts.googleapis.com rel=preconnect>
      <link
         rel=stylesheet id=elementor-global-css  href='<?php echo site_url(); ?>/wp-content/cache/autoptimize/css/autoptimize_single_2a6ec673f9e9b8e6ddbb084cc734124e.css' type=text/css media=all>
      <link
         rel=stylesheet id=elementor-post-5557-css  href='<?php echo site_url(); ?>/wp-content/cache/autoptimize/css/autoptimize_single_aceba08c7b3b8aacc3973fde3e000873.css' type=text/css media=all>
      <!-- <script src='wp-includes/js/jquery/jquery.js'></script>  -->
      <link
         rel=https://api.w.org/ href=https://www.blendr.io/wp-json/ >
      <link
         rel=EditURI type=application/rsd+xml title=RSD href=https://www.blendr.io/xmlrpc.php?rsd>
      <link
         rel=wlwmanifest type=application/wlwmanifest+xml href=https://www.blendr.io/wp-includes/wlwmanifest.xml>
      <meta
         name=generator content="WordPress 5.4.2">
      <link
         rel=shortlink href=https://www.blendr.io/ >
      <link
         rel=alternate type=application/json+oembed href="https://www.blendr.io/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.blendr.io%2F">
      <link
         rel=alternate type=text/xml+oembed href="https://www.blendr.io/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.blendr.io%2F&#038;format=xml">
      <meta
         property=fb:pages content>
      <link
         rel=icon href=<?php echo site_url(); ?>/wp-content/uploads/2017/11/cropped-favicon-512-512-32x32.png sizes=32x32>
      <link
         rel=icon href=<?php echo site_url(); ?>/wp-content/uploads/2017/11/cropped-favicon-512-512-192x192.png sizes=192x192>
      <link
         rel=apple-touch-icon href=<?php echo site_url(); ?>/wp-content/uploads/2017/11/cropped-favicon-512-512-180x180.png>
      <meta
         name=msapplication-TileImage content=https://www.blendr.io/<?php echo site_url(); ?>/wp-content/uploads/2017/11/cropped-favicon-512-512-270x270.png>
      <link rel=stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- <link rel=stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/js/all.min.js"> -->
      <link rel=stylesheet href="<?php echo site_url(); ?>/wp-content/themes/blend/css/custom.css">
   <body <?php body_class(); ?>>
      <nav
         class="navbar navbar-default navbar-fixed-top ">
         <div class=container>
            <div class="site-header">
               <div class=navbar-header>
                  <button type=button class="navbar-toggle collapsed" data-toggle=collapse data-target=#bs-example-navbar-collapse-1 aria-expanded=false>
                  <span class=sr-only>Toggle navigation</span>
                  <span class=icon-bar></span>
                  <span class=icon-bar></span>
                  <span class=icon-bar></span>
                  </button>
                
               </div>
               <a class=navbar-brand href="<?php echo site_url(); ?>" >
                  <img src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Logicon_Systems_Logo.png style="height: 38px;">
               </a>
               <div
                  class="collapse navbar-collapse" id=bs-example-navbar-collapse-1>

                  <?php

                     wp_nav_menu( array(
                         'menu' => 'header_menu'
                     ) );


                  ?>


                 <!--  <ul
                     id=menu-top-menu class="nav navbar-nav">
                     <li
                        itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-1869 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1869 dropdown">
                        <a
                           title=Platform href=# data-toggle=dropdown class=dropdown-toggle aria-haspopup=true>Platform <span
                           class=caret></span></a>
                        <ul
                           role=menu class=" dropdown-menu" >
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-3878 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3878"><a
                              title=Overview href=integration-platform.html >Overview</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-5062 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5062"><a
                              title="Connectivity layer" href=api-connectors.html >Connectivity layer</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-3877 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3877"><a
                              title="Integration layer" href=integration-builder.html >Integration layer</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-3879 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3879"><a
                              title="UI &amp; embedding layer" href=embed-integrations.html >UI &#038; embedding layer</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-5132 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5132"><a
                              title="Management layer" href=manage-integrations.html >Management layer</a></li>
                        </ul>
                     </li>
                     <li
                        itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-2849 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2849 dropdown">
                        <a
                           title=Solutions href=# data-toggle=dropdown class=dropdown-toggle aria-haspopup=true>Solutions <span
                           class=caret></span></a>
                        <ul
                           role=menu class=" dropdown-menu" >
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-2850 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2850"><a
                              title="Event Tech SaaS" href=event-technology-integration.html >Event Tech SaaS</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-511 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-511"><a
                              title="Martech SaaS" href=integrate-saas-platform.html >Martech SaaS</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-5171 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5171"><a
                              title="Cloud Communications" href=integrate-voip.html >Cloud Communications</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-6526 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6526"><a
                              title="AI SaaS" href=ai-saas-integrations.html >AI SaaS</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-7302 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7302"><a
                              title="Accounting Software" href=index.html >Accounting Software</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-4956 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4956"><a
                              title="Other B2B SaaS" href=index.html >Other B2B SaaS</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-7446 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7446"><a
                              title="HRM platforms" href=index.html >HRM platforms</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-7009 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7009"><a
                              title="Integrators &amp; agencies" href=index.html >Integrators &#038; agencies</a></li>
                        </ul>
                     </li>
                     <li
                        itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-6865 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6865"><a
                        title=Connectors href=/logi/connects/>Connectors</a></li>
                        <li id="logo-wrap"><a class=navbar-brand href="<?php echo site_url(); ?>" >
                  <img src=<?php echo site_url(); ?>/wp-content/uploads/2020/10/Logicon_Systems_Logo.png style="height: 38px;">
                  </a></li>
                     <li
                        itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-5148 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-5148 dropdown">
                        <a
                           title=Resources href=# data-toggle=dropdown class=dropdown-toggle aria-haspopup=true>Resources <span
                           class=caret></span></a>
                        <ul
                           role=menu class=" dropdown-menu" >
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-7564 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7564"><a
                              title=Documentation href=index.html >Documentation</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-7505 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7505"><a
                              title="Compliance &amp; data security" href=index.html >Compliance &#038; data security</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-7565 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7565"><a
                              title=Training href=index.html >Training</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-7566 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7566"><a
                              title=Webinars href=index.html >Webinars</a></li>
                           <li
                              itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-7567 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7567"><a
                              title=Blog href=index.html >Blog</a></li>
                        </ul>
                     </li>
                     <li
                        itemscope=itemscope itemtype=https://www.schema.org/SiteNavigationElement id=menu-item-5206 class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5206"><a
                        title=Pricing href=index.html >Pricing</a></li> -->
                  </ul>
                  <a
                     href=# class="btn btn-lg btn-transparent visible-lg-block" target=_blank style="float:right; border-color: white; border-style: solid; border-width: 2px; margin-right: 20px; color: white;">Get a demo</a>
                  <a
                     href=# target=_blank class="btn btn-lg btn-transparent visible-lg-block" style="float:right; border-color: white; border-style: solid; border-width: 2px; color: white;">Login</a>
               </div>
            </div>
         </div>
      </nav>